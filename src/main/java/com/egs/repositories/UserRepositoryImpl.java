package com.egs.repositories;

import com.egs.entity.User;
import com.egs.fakestorage.FakeStorage;

import java.util.List;

public class UserRepositoryImpl implements UsersRepository {
    @Override
    public List<User> findAll() {
        return FakeStorage.storage().users();
    }

    @Override
    public void save(User user) {
        FakeStorage.storage().users().add(user);
    }

    @Override
    public boolean isExist(String username, String password) {
        for (User user : FakeStorage.storage().users()) {
            if (user.getUserName().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
