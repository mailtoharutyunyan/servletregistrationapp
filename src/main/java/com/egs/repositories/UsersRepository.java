package com.egs.repositories;

import com.egs.entity.User;

import java.util.List;

public interface UsersRepository {

    List<User> findAll();

    void save(User user);

    boolean isExist(String username, String password);


}