package com.egs.fakestorage;

import com.egs.entity.User;

import java.util.ArrayList;
import java.util.List;

public class FakeStorage {

    private static final FakeStorage storage;

    static {
        storage = new FakeStorage();
    }

    private List<User> users;

    private FakeStorage() {
        this.users = new ArrayList<>();
        User user1 = new User("Arayik", "myUsername", "myPassword");
        User user2 = new User("Arayik1", "myUsername1", "myPassword1");
        User user3 = new User("ME", "root", "root");

        users.add(user1);
        users.add(user2);
        users.add(user3);
    }

    public static FakeStorage storage() {
        return storage;
    }

    public List<User> users() {
        return users;
    }
}